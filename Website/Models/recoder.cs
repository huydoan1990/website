﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Website.Models
{
    [Table("recoder")]
    public class recoder
    {
        [Key]
        public DateTime Thoigian { get; set; }
        public string Kenh_1 { get; set; }
        public string Tag_1 { get; set; }
        public string Tag_2 { get; set; }
        public string Tag_3 { get; set; }
        public string Tag_4 { get; set; }
    }
}