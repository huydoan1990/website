﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Website.Models
{
    [Table("hienthi")]
    public class Hienthi
    {
        [Key]
        [Required(ErrorMessage = "Vui lòng nhập tên Kênh.")]
        public string Kenh { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập giá trị Tag 1.")]
        [RegularExpression("^[0-9]*\\.?[0-9]*$", ErrorMessage = "Vui lòng nhập số.")]
        public string Tag1 { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập giá trị Tag 2.")]
        [RegularExpression("^[0-9]*\\.?[0-9]*$", ErrorMessage = "Vui lòng nhập số.")]
        public string Tag2 { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập giá trị Tag 3.")]
        [RegularExpression("^[0-9]*\\.?[0-9]*$", ErrorMessage = "Vui lòng nhập số.")]
        public string Tag3 { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập giá trị Tag 4.")]
        [RegularExpression("^[0-9]*\\.?[0-9]*$", ErrorMessage = "Vui lòng nhập số.")]
        public string Tag4 { get; set; }
    }
}