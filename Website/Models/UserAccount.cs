﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Website.Models
{
    [Table("useraccount")]
    public class UserAccount
    {
        [Key]
        [Required(ErrorMessage = "Vui lòng nhập tên đăng nhập.")]
        [MinLength(3, ErrorMessage = "Tên đăng nhập ít nhất 3 kí tự.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập mật khẩu.")]
        [MinLength(6 , ErrorMessage = "Mật khẩu ít nhất 6 kí tự.")]
        [DataType(DataType.Password)]
        public string Pass { get; set; }

        [Required(ErrorMessage = "Vui lòng xác nhận mật khẩu.")]
        [DataType(DataType.Password)]
        [Compare("Pass", ErrorMessage = "Mật khẩu xác nhận không khớp.")]
        [NotMapped]
        public string ConfirmPass { get; set; }
    }

    public class UserAccLogin
    {
        [Required(ErrorMessage = "Vui lòng nhập tên đăng nhập.")]
        [MinLength(3, ErrorMessage = "Tên đăng nhập ít nhất 3 kí tự.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập mật khẩu.")]
        [MinLength(6, ErrorMessage = "Mật khẩu ít nhất 6 kí tự.")]
        [DataType(DataType.Password)]
        public string Pass { get; set; }
    }
}