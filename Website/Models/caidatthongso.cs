﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Website.Models
{
    [Table("cai_dat_thong_so")]
    public class caidatthongso
    {
        [Key]
        [Required(ErrorMessage = "Vui lòng nhập tên Kênh.")]
        public string Kenh_1 { get; set; }
        //[Required(ErrorMessage = "Vui lòng nhập giá trị Tag 1.")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Vui lòng nhập số.")]
        public string Tag_1 { get; set; }
        public string Chot_tag1 { get; set; }

        //[Required(ErrorMessage = "Vui lòng nhập giá trị Tag 2.")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Vui lòng nhập số.")]
        public string Tag_2 { get; set; }
        public string Chot_tag2 { get; set; }
        public string Button_1 { get; set; }
        public string Chot_button1 { get; set; }
    }
}