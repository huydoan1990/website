﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Website.Models
{
    public class Location
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập tên địa điểm.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập tọa độ.")]
        [RegularExpression("^[0-9]*\\.?[0-9]*$", ErrorMessage = "Vui lòng nhập số.")]
        public string Latitude { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập tọa độ.")]
        [RegularExpression("^[0-9]*\\.?[0-9]*$", ErrorMessage = "Vui lòng nhập số.")]
        public string Longitude { get; set; }
        public string Description { get; set; }
        [Required(ErrorMessage = "Vui lòng chọn thứ tự.")]
        public int Position { get; set; }
    }
}