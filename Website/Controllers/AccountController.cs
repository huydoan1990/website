﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Website.Models;

namespace Website.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Login()
        {
            return View();
        }

        // POST: /Account/Login
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(UserAccLogin objUser)
        {
            if (ModelState.IsValid)
            {
                using (DBModel db = new DBModel())
                {
                    var obj = db.useraccount.Where(a => a.Name.Equals(objUser.Name) && a.Pass.Equals(objUser.Pass)).FirstOrDefault();
                    if (obj != null)
                    {
                        Session["UserID"] = obj.Name.ToString();
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        ViewBag.Message = "Email hoặc mật khẩu không chính xác.";
                        return View();
                    }
                }
            }
           
            return View(objUser);
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(UserAccount objUser)
        {
            if (ModelState.IsValid)
            {
                using (DBModel db = new DBModel())
                {
                    var obj = db.useraccount.Where(a => a.Name.Equals(objUser.Name)).FirstOrDefault();
                    if (obj != null)
                    {
                        ViewBag.Message = "Tài khoản đã tồn tại.";
                        return View();
                    }
                    else
                    {
                        try
                        {
                            db.useraccount.Add(objUser);
                            db.SaveChanges();
                            return RedirectToAction("Login", "Account");
                        } 
                        catch(Exception exception)
                        {
                            ViewBag.Message = exception;
                            return View();
                        }
                    }
                }
            }
            return View(objUser);
        }
    }
}