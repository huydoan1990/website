﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Website.Models;
using PagedList;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;

namespace Website.Controllers
{
    public class HomeController : Controller
    {
        // GET: Menu
        
        public ActionResult Index()
        {
            if (Session["UserID"] != null)
            {
                ViewBag.Email = Session["Email"];
                using (DBModel db = new DBModel())
                {
                    var obj = db.hienthi.Where(a => a.Kenh.Equals("1")).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.Tag1 = Convert.ToDecimal(obj.Tag1).ToString("#,##0");
                        obj.Tag2 = Convert.ToDecimal(obj.Tag2).ToString("#,##0");
                        obj.Tag3 = Convert.ToDecimal(obj.Tag3).ToString("#,##0");
                        obj.Tag4 = Convert.ToDecimal(obj.Tag4).ToString("#,##0");
                        ViewBag.HienThi = obj;
                    }
                }
                ViewBag.Recoder = new recoder();
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            //return View();
        }

        public ActionResult Trang2()
        {
            if (Session["UserID"] != null)
            {
                string markers = "[";
                using (DBModel db = new DBModel())
                {
                    var locations = db.location.ToList();
                    var dataHienthi = db.hienthi.Where(a => a.Kenh.Equals("1")).FirstOrDefault();
                    if(locations != null && dataHienthi != null)
                    {
                        foreach (var item in locations)
                        {
                            markers += "{";
                            markers += string.Format("'title': '{0}',", item.Name);
                            markers += string.Format("'lat': '{0}',", item.Latitude);
                            markers += string.Format("'lng': '{0}',", item.Longitude);
                            markers += string.Format("'position': '{0}',", Convert.ToInt16(item.Position));
                            markers += string.Format("'description': '{0}',", item.Description);
                            markers += string.Format("'tag1': '{0}',", Convert.ToDecimal(dataHienthi.Tag1).ToString("#,##0"));
                            markers += string.Format("'tag2': '{0}',", Convert.ToDecimal(dataHienthi.Tag2).ToString("#,##0"));
                            markers += string.Format("'tag3': '{0}',", Convert.ToDecimal(dataHienthi.Tag3).ToString("#,##0"));
                            markers += string.Format("'tag4': '{0}'", Convert.ToDecimal(dataHienthi.Tag4).ToString("#,##0"));
                            markers += "},";
                        }
                    }
                }

                markers += "];";
                ViewBag.Markers = markers;
                ViewBag.Recoder = new recoder();
                ViewBag.Hienthi = new Hienthi();
                return View();
                

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        [HttpGet]
        public JsonResult GetHienThi()
        {
            using (DBModel db = new DBModel())
            {
                var obj = db.hienthi.Where(a => a.Kenh.Equals("1")).FirstOrDefault();
                //if (obj != null)
                //{
                //    //obj.Tag1 = Convert.ToDecimal(obj.Tag1).ToString("#,##0");
                //    //obj.Tag2 = Convert.ToDecimal(obj.Tag2).ToString("#,##0");
                //    //obj.Tag3 = Convert.ToDecimal(obj.Tag3).ToString("#,##0");
                //    //obj.Tag4 = Convert.ToDecimal(obj.Tag4).ToString("#,##0");
                //}
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetChartData()
        {
            using (DBModel db = new DBModel())
            {
                var recoders = db.recoder.Where(a => a.Kenh_1.Equals("kenh1")).OrderByDescending(x => x.Thoigian)
                             .Take(1).ToList().FirstOrDefault();
                return Json(recoders, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Map()
        {
            if (Session["UserID"] != null)
            {
                string markers = "[";
                using (DBModel db = new DBModel())
                {
                    var locations = db.location.ToList();
                    foreach (var item in locations)
                    {
                        markers += "{";
                        markers += string.Format("'title': '{0}',", item.Name);
                        markers += string.Format("'lat': '{0}',", item.Latitude);
                        markers += string.Format("'lng': '{0}',", item.Longitude);
                        markers += string.Format("'description': '{0}'", item.Description);
                        markers += "},";
                    }
                }

                markers += "];";
                ViewBag.Markers = markers;
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public ActionResult TableList(int? page, DateTime? fromDate, DateTime? toDate)
        {
            
            if (Session["UserID"] != null)
            {
                
                using (DBModel db = new DBModel())
                {
                    var recoders = new List<recoder>();
                    if (fromDate.HasValue && toDate.HasValue)
                    {
                        toDate = toDate.GetValueOrDefault(DateTime.Now.Date).Date.AddHours(23).AddMinutes(59);
                        recoders = db.recoder.Where(x => x.Thoigian <= toDate && x.Thoigian >= fromDate).ToList();
                        ViewBag.Recoders = recoders;
                        ViewBag.fromDate = fromDate;
                        ViewBag.toDate = toDate;
                        if (page == null)
                        {
                            page = 1;
                        }
                        int pageSize = 20;
                        int pageNumber = (page ?? 1);
                        Session["recoders"] = recoders;
                        return View(recoders.ToPagedList(pageNumber, pageSize));
                    }
                    else
                    {
                        if (!fromDate.HasValue) fromDate = DateTime.Now.Date;
                        if (!toDate.HasValue) toDate = fromDate.GetValueOrDefault(DateTime.Now.Date).Date.AddDays(1);
                        if (toDate < fromDate) toDate = fromDate.GetValueOrDefault(DateTime.Now.Date).Date.AddDays(1);
                        recoders = db.recoder.ToList();
                        ViewBag.Recoders = recoders;
                        ViewBag.fromDate = fromDate;
                        ViewBag.toDate = toDate;
                        if (page == null)
                        {
                            page = 1;
                        }
                        int pageSize = 20;
                        int pageNumber = (page ?? 1);
                        Session["recoders"] = recoders;
                        return View(recoders.ToPagedList(pageNumber, pageSize));
                        //return View(recoders);
                    }
                    //Phan trang
                    if(page == null)
                    {
                        page = 1;
                    }
  
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
        public ActionResult ExportToExcel()
        {
            var recoders = (List<recoder>)Session["recoders"];
            GridView gv = new GridView();
            gv.DataSource = recoders;
            gv.DataBind();

            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/ms-excel";
            Response.AddHeader("content-disposition", "attachment;filename=StudentList.xls");
            Response.Charset = "";

            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            gv.RenderControl(htw);

            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();

            return RedirectToAction("TableList");
        }
    }
}