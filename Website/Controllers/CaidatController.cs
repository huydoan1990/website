﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Website.Models;

namespace Website.Controllers
{
    public class CaidatController : Controller
    {
        [HttpPost]
        public JsonResult UpdateThongSo(string tag1, string tag2, string state)
        {
            using (DBModel db = new DBModel())
            {
                var updateCaidat = db.caidat.Where(a => a.Kenh_1.Equals("kenh1")).FirstOrDefault();
                if (updateCaidat != null)
                {
                    if (!string.IsNullOrWhiteSpace(tag1))
                    {
                        updateCaidat.Tag_1 = tag1;
                        updateCaidat.Chot_tag1 = "1";
                    }
                    if (!string.IsNullOrWhiteSpace(tag2))
                    {
                        updateCaidat.Tag_2 = tag2;
                        updateCaidat.Chot_tag2 = "2";
                    }
                    if (!string.IsNullOrWhiteSpace(state))
                    {
                        updateCaidat.Button_1 = (state == "on") ? "1" : "0";
                        updateCaidat.Chot_button1 = (state == "on") ? "3" : "1";
                    }
                    db.SaveChanges();
                }
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            
        }

    }
}