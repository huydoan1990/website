﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Website.Models;

namespace Website.Controllers
{
    public class LocationController : Controller
    {
        // GET: Location
        public ActionResult Create()
        {
            if (Session["UserID"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Location objLocation)
        {
            if (ModelState.IsValid)
            {
                using (DBModel db = new DBModel())
                {
                    var obj = db.location.Where(a => a.Name.Equals(objLocation.Name)).FirstOrDefault();
                    if (obj != null)
                    {
                        ViewBag.Message = "Địa điếm đã tồn tại.";
                        return View();
                    }
                    var objPos = db.location.Where(a => a.Position.Equals(objLocation.Position)).FirstOrDefault();
                    if (objPos != null)
                    {
                        ViewBag.Message = "Thứ tự đã được dùng cho địa điểm khác.";
                        return View();
                    }
                    else
                    {
                        try
                        {
                            var createObj = new Location();
                            createObj.Latitude = objLocation.Latitude;
                            createObj.Longitude = objLocation.Longitude;
                            createObj.Name = objLocation.Name;
                            createObj.Position = objLocation.Position;
                            createObj.Description = objLocation.Description;
                            db.location.Add(createObj);
                            db.SaveChanges();
                            ViewBag.Message = "Thêm địa điểm thành công.";
                            ModelState.Clear();
                            return View(new Location());
                        }
                        catch (Exception exception)
                        {
                            ViewBag.Message = exception;
                            return View();
                        }
                    }
                }
            }
            return View();
        }
    }
}